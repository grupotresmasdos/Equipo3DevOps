provider "aws" {
    region = "sa-east-1"
    access_key = "var.my_access_key"
    secret_key = "var.my_secret_key"
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.example.id
  instance_id = aws_instance.InstanciaCloudDevopsTeamTree.id
}

resource "aws_instance" "InstanciaCloudDevopsTeamTree" {
  ami = "ami-0e3163696b67521ae"
  instance_type = "t2.micro"
  availability_zone = "sa-east-1c"
  key_name = "windows-terraform"
  security_groups = ["${aws_security_group.allow_rdp.name}"]

  tags = {
      Name = "Instancia Cloud DevopsTeamTree"
      Año = "2022"
      Sistema_Operativo = "Windows Server 2016"
      Nombre_Equipo = "TreeTeam"
      Carrera = "DevOps BootCamp"
      Institución = "Educación IT"
      Proyecto = "Carrera Cloud Devops"
      Entorno = "Producción"


  }
  
}

resource "aws_ebs_volume" "example" {
  availability_zone = "sa-east-1c"
  size              = 30
}

resource "aws_security_group" "allow_rdp" {
        name = "allow_rdp"
        description = "security group para instancias EC2"

ingress {
from_port = 3389
to_port = 3389
protocol = "tcp"
cidr_blocks = ["0.0.0.0/0"] 
}

ingress {

from_port = 80
to_port = 80
protocol = "TCP"
cidr_blocks = ["0.0.0.0/0"]
}

egress {
from_port = 0
to_port = 0
protocol = "-1"
cidr_blocks = ["0.0.0.0/0"]
}


}
